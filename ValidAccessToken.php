<?php

## Include the Dropbox SDK libraries
require_once "dropbox-sdk/autoload.php";
use \Dropbox as dbx;

echo "Retrieving Access Token...\n";
$accessToken = getAccessToken();
echo "The config.json file has a working Access Token\n";

function getAccessToken() {
	$accessToken = '';
	
	$filepath = 'config.json';
	$str = file_get_contents($filepath);
	$jsonArr = json_decode($str, TRUE);
	
	$appInfo = dbx\AppInfo::loadFromJsonFile($filepath);
	$dbxConfig = new dbx\Config($appInfo, "PHP-Example/1.0");
	
	try {
		$accessToken = new dbx\AccessToken($jsonArr['access_token']['key'], $jsonArr['access_token']['secret']);
		
		try {
			$dbxClient = new dbx\Client($dbxConfig, $accessToken);
			$accountInfo = $dbxClient->getAccountInfo();
			
			echo "Validated that current Access Token works.\n";
		}
		catch (Exception $e1) {
			echo "Invalid Access Token.  Proceeding to create new one...\n";
			throw $e1;
		}
	}
	catch (Exception $e2) {
		$webAuth = new dbx\WebAuth($dbxConfig);
		list($requestToken, $authorizeUrl) = $webAuth->start(null);
		
		echo "1. Go to: " . $authorizeUrl . "\n";
		echo "2. Click \"Allow\" (you might have to log in first).\n";
		echo "3. Hit ENTER to continue.\n";
		fgets(STDIN);

		try {
			list($accessToken, $dropboxUserId) = $webAuth->finish($requestToken);
			$jsonArr['access_token']['key'] = $accessToken->getKey();
			$jsonArr['access_token']['secret'] = $accessToken->getSecret();
			file_put_contents($filepath, json_encode($jsonArr));
			echo "Updated config.json with new Access Token\n";
		}
		catch (Exception $e3) {
			$accessToken = getAccessToken();
		}
	}
	
	return $accessToken;
}
?>
