# DropBox API Setup

1. Go to [http://www.dropbox.com/developers](http://www.dropbox.com/developers).

2. In the left-hand navigation, click the 'App console' link

3. Click the 'Create an app' button

4. Choose the 'Core' option

5. Under 'App name', choose a name for the app.

6. Under 'Permission type', make sure 'App folder' is selected.

7. Click 'Create app' button.

8. Be sure to take note of the provided 'App key' and 'App secret'