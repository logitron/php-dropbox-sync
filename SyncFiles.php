<?php

## Include the Dropbox SDK libraries
require_once "dropbox-sdk/autoload.php";
use \Dropbox as dbx;

$config_path = 'config.json';
$configStr = file_get_contents($config_path);
$config = json_decode($configStr, TRUE);

$appInfo = dbx\AppInfo::loadFromJsonFile($config_path);
$dbxConfig = new dbx\Config($appInfo, "PHP-Example/1.0");

// Retrieve Access Token from config.json
$accessToken = new dbx\AccessToken(
	$config['access_token']['key'],
	$config['access_token']['secret']);

// Create DropBox Client
$dbxClient = new dbx\Client($dbxConfig, $accessToken);

$cursor = $config['cursor'];
if ($cursor == '') $cursor = null;

$delta = $dbxClient->getDelta($cursor);
$config['cursor'] = $delta["cursor"];
file_put_contents($config_path, json_encode($config));

chdir($config['localPath']);
foreach ($delta['entries'] as $entry) {
	if ($entry[1] == null)
		DeleteContent($entry[0]);
	elseif (!$entry[1]['is_dir'])
		ProcessContent($dbxClient, $entry[0]);	
}

function DeleteContent($content) {
	if (!unlink("." . $content))
		rmdir("." . $content);
}
	
function ProcessContent($dbxClient, $content) {
	$path = explode("/", $content);
	$localPath = getcwd();
	
	for ($i = 0; $i < (count($path) - 1); $i++) {
		if ($path[$i] == '' || file_exists($path[$i]))
		{
			if ($path[$i] != '')
				chdir($path[$i]);
				
			continue;
		}
		
		mkdir($path[$i]);
		chdir($path[$i]);
	}
	
	chdir($localPath);
	
	$f = fopen("." . $content, "w+b");
	$fileMetadata = $dbxClient->getFile($content, $f);
	fclose($f);
}
